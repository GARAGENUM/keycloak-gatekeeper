# KEYCLOAK GATEKEEPER

Proxy pour service redirigeant vers Keycloak pour authentification

![](docs/principe.png)

## PRE REQUIS :paperclip:

- Docker / docker compose :whale:
- Instance Keycloak

## CONFIGURATION :wrench:

### GATEKEEPER

#### Config file (TO DO):
- Renseigner gatekeeper-config.yml:
```bash
# TO DO
```

#### DOCKER COMPOSE
- Renseigner les champs suivants:
```bash
      - "--discovery-url=https://<MON_KEYCLOAK>/auth/realms/<MON_REALM>/.well-known/openid-configuration"
      - "--client-id=<CLIENT_ID>"
      - "--client-secret=<CLIENT_SECRET>"
      # le port écouté doit être le même que celui mappé sur l'hôte
      - "--listen=0.0.0.0:8880"
      - "--upstream-url=http://<MON_SERVICE>:<PORT>"
```

:warning: Le port renseigné dans "--upstream-url=http://<MON_SERVICE>:<PORT>" doit correspondre au port exposé par le conteneur.

:skull: Les port mappés pour le service gatekeeper doivent être les mêmes et doivent correspondre avec celui renseigné au niveau de "--listen=0.0.0.0:8880"

### KEYCLOAK :key:

- Créer un client:

![keycloak illustration](docs/keycloak-1.png)

![keycloak illustration](docs/keycloak-2.png)

> Le client secret se trouve dans l'inglet "credentials"

- Créer une audience:

![keycloak illustration 2](docs/keycloak-3.png)

![keycloak illustration](docs/keycloak-4.png)

## UTILISATION :rocket:

- Cloner le projet:
```bash
git clone --submodules https://git.legaragenumerique.fr/GARAGENUM/keycloak-gatekeeper
cd keycloak-gatekeeper
```

- Lancer les conteneurs:
```bash
docker compose up -d
```

## TO DO :bookmark_tabs:

- [ ] Injecter gatekeeper-config.yml via volumes
- [X] Keycloak client config illustrations
- [X] Illustration workflow